/**
 * For a detailed explanation regarding each configuration property, visit:
 * https://jestjs.io/docs/configuration
 */

import type {Config} from 'jest';

const config: Config = {
  clearMocks: true,
  collectCoverage: true,
  collectCoverageFrom: ["./src/component/**.{ts,tsx}", "./src/layout/**.{ts,tsx}"],
  globals: {
    fetch: global.fetch,
  },
  setupFilesAfterEnv: ["@testing-library/jest-dom", "./setupTests.ts"],
  transform: {
    "^.+\\.(ts|tsx|js|jsx)$": "ts-jest"
  },
  testEnvironment: "jest-environment-jsdom"
};

export default config;
