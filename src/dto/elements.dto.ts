export interface ElementDto {
    createdAt?: string;
    name: string;
    avatar: string;
    id?: string;
  }