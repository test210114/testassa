import { configureStore } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/query";
import { elementsApi } from "./api/element.api";
import elementReducer from './slices/element'


export const store = configureStore({
  reducer: {
    elements: elementReducer,
    [elementsApi.reducerPath]: elementsApi.reducer,
},
  middleware: (getDefaultMiddleware) =>
    {
      return getDefaultMiddleware({
        serializableCheck: false,
      })
        .concat(elementsApi.middleware)
    }
});

setupListeners(store.dispatch);

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
