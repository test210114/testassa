import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { ElementDto } from "../../dto/elements.dto";


interface IElementState {
    data: ElementDto[]
}

const elementsValue : ElementDto[] = [];


const initialState: IElementState = {
  data: elementsValue,
};

export const elementsSlice = createSlice({
  name: "elements",
  initialState,
  reducers: {
    setElementsValue(state, action: PayloadAction<ElementDto[]>) {
      return {
        ...state,
        data: action.payload,
      };
    },
    addElementValue(state, action: PayloadAction<ElementDto>) {
      action.payload.id = (state.data.length + 1).toString();
      action.payload.createdAt = new Date().toISOString();
      state.data = state.data.concat(action.payload)
      return state
    }
  },
});

export const { setElementsValue, addElementValue } = elementsSlice.actions;
export default elementsSlice.reducer;
