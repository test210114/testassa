import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { ElementDto } from '../../dto/elements.dto';

export const elementsApi = createApi({
  reducerPath: 'elementsApi',
  baseQuery: fetchBaseQuery({ baseUrl: 'https://6172cfe5110a740017222e2b.mockapi.io' }),
  endpoints: (builder) => ({
    getElementList: builder.mutation<ElementDto[] | undefined, string | undefined>({
        query: () => 'elements/',
    })
  }),
})

export const { useGetElementListMutation } = elementsApi