import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { AppDispatch } from "../store/store";
import { addElementValue } from "../store/slices/element";
type FormType = { show: boolean, callback: () => void};
export function Form({ show, callback }: FormType) {
    const dispatch = useDispatch<AppDispatch>();
    const nameRef = React.useRef<HTMLInputElement | null>(null);
    const avatarRef = React.useRef<HTMLInputElement | null>(null);

    useEffect(() => {
        if(nameRef?.current)
            nameRef.current.value = '';
        
        if(avatarRef?.current)
            avatarRef.current.value = '';
    }, [show]);

    const onSave = (data: any) => {
        data.preventDefault();
        const name = nameRef?.current?.value;
        const avatar = avatarRef?.current?.value;
        if(!name || !avatar)
            return;

        dispatch(addElementValue({name: name, avatar: avatar}));
        callback();
    };
    return (
        <div>
            <form onSubmit={onSave} data-testid="form">
                <label>Name</label>
                <input type="text" ref={nameRef} data-testid="name" />
                <label>Avatar</label>
                <input type="text" ref={avatarRef} data-testid="avatar" />
                <input type="submit" className="submitButton" />
            </form>
        </div>
    )
}