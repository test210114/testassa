import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { RootState } from "../store/store";
import { ElementDto } from "../dto/elements.dto";
import { useNavigate } from "react-router-dom";
import { Modal } from "./modal";
import { Form } from "./form";

export function Task() {

    const navigate = useNavigate();
    const dataList = useSelector((state: RootState) => state.elements);
    const [openModal, setOpenModal] = useState<boolean>(false);
    const [elementInfo, setElementInfo] = useState<ElementDto[] | undefined>([]);

    useEffect(() => {
        const data = !dataList.data || !dataList.data.length ? [] : [...dataList.data]
        data.sort((a, b) => !a.id || !b.id ? 0 : (+b.id) - (+a.id));
        setElementInfo(data);
    }, [dataList]);

    const toHome = () => {
        navigate(`/Home`);
    };

    const addData = () => {
        setOpenModal(true);
    };

    const onClose = () => {
        setOpenModal(false);
    };

    const renderList = () => {
        return (
            <div>
                <div className="center-container">
                    <button className="main-button" onClick={toHome}>Regresar</button>
                </div>
                <div className="center-container">
                    <button className="main-button" onClick={addData}>Agregar</button>
                </div>
                <div className="center-container">
                    <table>
                        {elementInfo?.map((data, index) => (
                            <tr>
                                <th>{data.id}</th>
                                <th>{data.name}</th>
                                <th>{data.createdAt}</th>
                            </tr>
                        ))}
                    </table>
                </div>
            </div>
        );
    };
    return (
        <div>
            {renderList()}
            <Modal open={openModal} closeModal={onClose}>
                <Form show={openModal} callback={onClose}></Form>
            </Modal>
        </div>
    )
}