import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "../store/store";
import { ElementDto } from "../dto/elements.dto";
import { useGetElementListMutation } from "../store/api/element.api";
import { setElementsValue } from "../store/slices/element";
import { useNavigate } from "react-router-dom";

export function List() {
    
    const navigate = useNavigate();
    const dataList = useSelector((state: RootState) => state.elements);
    const dispatch = useDispatch<AppDispatch>();
    const [elementInfo, setElementInfo] = useState<ElementDto[] | undefined>(undefined);
    const [loading, setLoading] = useState<boolean>(false);
    const [fetchElementList] = useGetElementListMutation();

    useEffect(() => {
        setLoading(true);
        fetchElementList('').unwrap().then((response: any) => {
            dispatch(setElementsValue(response));
            setLoading(false);
        });
      }, []);

    useEffect(() => {
        setElementInfo(!dataList.data || !dataList.data.length ? [] : dataList.data);
    }, [dataList]);

    const renderLoading = () => {
        return loading ? (<div className="loader"></div>) : (<div></div>);
    };

    const toHome = () => {
        navigate(`/Home`);
    };

    const renderList = () => {
        return !loading ? 
        (
            <div>
                <div className="center-container">
                    <button className="main-button" onClick={toHome}>Regresar</button>
                </div>
                <div className="center-container">
                    <table>
                    {elementInfo?.map((data, index) => (
                    <tr>
                        <th className="image-cell">
                            <img src={data.avatar}/>
                        </th>
                        <th>{data.name}</th>
                    </tr>
                    ))}
                    </table>
                </div>
            </div>
        ) 
        : (<div></div>);
    };
    return (
        <div>
            {renderLoading()}
            {renderList()}
        </div>
    )
}