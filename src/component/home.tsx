import React from "react";
import { useNavigate } from "react-router-dom";

export function Home() {
    
    const navigate = useNavigate();
    
    const toTask = () => {
        navigate(`/Task`);
    };
    const toList = () => {
        navigate(`/List`);
    }
    return (
        <div>
            <div className="center-container">
                <button className="main-button" onClick={toTask}>Task</button>
            </div>
            <div className="center-container">
                <button className="main-button" onClick={toList}>List</button>
            </div>
        </div>
    )
}