// Modal as a separate component
import React, { ReactNode, useEffect, useRef } from "react";
type ModalType = { open: boolean, closeModal: () => void, children: ReactNode };
export function Modal({ open, closeModal, children }: ModalType) {
    const ref = useRef<HTMLDialogElement | null>(null);
    useEffect(() => {
        if (open && ref.current?.showModal) 
            ref.current?.showModal();
        if (!open && ref.current?.close) 
            ref.current?.close();
    }, [open]);
    return (
        <dialog className="add-modal"
            ref={ref}
            onCancel={closeModal}
        >
            <button  className="close-button" onClick={closeModal}>
                X
            </button>
            {children}
        </dialog>
    );
}