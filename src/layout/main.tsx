import { Routes, Route, Navigate } from "react-router-dom";
import { Home } from '../component/home';
import { Task } from "../component/task";
import { List } from "../component/list";

export function MainLayout() {
    return (
        <Routes>
            <Route path={'/home'} element={<Home />} />
            <Route path={'/task'} element={<Task />} />
            <Route path={'/list'} element={<List />} />
            <Route
              path="*"
              element={<Navigate to={'/home'} replace />}
            />
        </Routes>
    )
}