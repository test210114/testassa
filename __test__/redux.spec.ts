import { store } from '../src/store/store';
import { setElementsValue, addElementValue } from '../src/store/slices/element';

test('Set data to store', () => {
  const data = [
      {id: '1', name: 'test', avatar: 'test'},
      {id: '2', name: 'test', avatar: 'test'},
      {id: '3', name: 'test', avatar: 'test'},
      {id: '4', name: 'test', avatar: 'test'}
  ];
  store.dispatch(setElementsValue(data)); 
  
  let state = store.getState().elements;
  expect(state.data.length).toEqual(data.length);
});

test('Add one element to store', () => {
  const data = [
      {id: '1', name: 'test', avatar: 'test'},
      {id: '2', name: 'test', avatar: 'test'},
      {id: '3', name: 'test', avatar: 'test'},
      {id: '4', name: 'test', avatar: 'test'}
  ];
  store.dispatch(setElementsValue(data)); 
  store.dispatch(addElementValue({id: '5', name: 'test', avatar: 'test'})); 
  let state = store.getState().elements;
  expect(state.data.length).toEqual(data.length + 1);
});