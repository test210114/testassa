import 'whatwg-fetch';
import React from "react";
import { render, fireEvent, act } from "@testing-library/react";
import '@testing-library/jest-dom';
import {List} from "../src/component/list";
import { renderWithProviders } from "./test-util";
import fetch from 'jest-fetch-mock';

const mockedUsedNavigate = jest.fn();
jest.mock('react-router-dom', () => ({
...jest.requireActual('react-router-dom') as any,
useNavigate: () => mockedUsedNavigate,
}));

jest.mock("react", () => {
    return {
      ...jest.requireActual("react"),
      useRef: () => {
        return React.createRef();
      }
    };
});

const mockSetState = jest.fn();
jest.mock('react', () => ({
    ...jest.requireActual('react'),
    useState: (initial: object) => [initial, mockSetState]
}));

const meta = {
  'Content-Type': 'application/json',
  Accept: '*/*',
  'Breaking-Bad': '<3'
};
const headers = new Headers(meta);
const ResponseInit = {
  status: 200,
  statusText: 'success',
  headers: headers
};
jest.spyOn(global, 'fetch')
  .mockImplementation(() => Promise.resolve(
    new Response(
      JSON.stringify([
        {id: '1', name: 'test', avatar: 'test'},
        {id: '2', name: 'test', avatar: 'test'},
        {id: '3', name: 'test', avatar: 'test'},
        {id: '4', name: 'test', avatar: 'test'}
      ]),
      ResponseInit
    )
  )
);


describe("<List />", () => {
    test("Test list element", async () => {
      const listElement = await act( async () => {
          return await renderWithProviders(<List />)
      });

      const backButton = listElement.getByText('Regresar');
      expect(backButton).toBeInTheDocument();
      fireEvent.click(backButton);
      expect(mockedUsedNavigate).toHaveBeenCalled();

    });
    
});