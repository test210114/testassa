import React from "react";
import { render, fireEvent, act } from "@testing-library/react";
import '@testing-library/jest-dom';
import {Form} from "../src/component/form";
import { renderWithProviders } from "./test-util";

const onClose = () => {
};

const mockSetState = jest.fn();
jest.mock('react', () => ({
    ...jest.requireActual('react'),
    useState: (initial: object) => [initial, mockSetState]
}));

describe("<Form />", () => {
    test("Test Form element", async () => {
        const formElement = await renderWithProviders(<Form show={true} callback={onClose}/>);
        const formButton = formElement.getByTestId('form');
        fireEvent.submit(formButton);
    });
});