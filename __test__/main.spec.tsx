import React from "react";
import { render, fireEvent, act } from "@testing-library/react";
import '@testing-library/jest-dom';
import {MainLayout} from "../src/layout/main";
import { BrowserRouter } from "react-router-dom";

const mockedUsedNavigate = jest.fn();
jest.mock('react-router-dom', () => ({
...jest.requireActual('react-router-dom') as any,
useNavigate: () => mockedUsedNavigate,
}));

describe("<MainLayout />", () => {
    test("Test MainLayout element", async () => {
        await render(
            <BrowserRouter>
                <MainLayout />
            </BrowserRouter>
        );

    });
});