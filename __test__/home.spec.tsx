import React from "react";
import { render, fireEvent, act } from "@testing-library/react";
import '@testing-library/jest-dom';
import {Home} from "../src/component/home";

const mockedUsedNavigate = jest.fn();
jest.mock('react-router-dom', () => ({
...jest.requireActual('react-router-dom') as any,
useNavigate: () => mockedUsedNavigate,
}));

describe("<Home />", () => {
    test("Test Home element", async () => {
        const homeElement = await render(<Home />);
        const taskButton = homeElement.getByText('Task');
        expect(taskButton).toBeInTheDocument();
        fireEvent.click(taskButton);
        expect(mockedUsedNavigate).toHaveBeenCalled();
        
        const listButton = homeElement.getByText('List');
        expect(listButton).toBeInTheDocument();
        fireEvent.click(listButton);
        expect(mockedUsedNavigate).toHaveBeenCalled();

    });
});