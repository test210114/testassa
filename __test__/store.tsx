import { configureStore } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/query";
import elementReducer from './element-test'
import { elementsApi } from '../src/store/api/element.api';

export const store = configureStore({
  reducer: {
    elements: elementReducer,
    [elementsApi.reducerPath]: elementsApi.reducer,
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(elementsApi.middleware),
});

setupListeners(store.dispatch);

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
