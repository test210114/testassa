import React from "react";
import { render, fireEvent, act } from "@testing-library/react";
import '@testing-library/jest-dom';
import {Modal} from "../src/component/modal";

const onClose = () => {
};

jest.mock("react", () => {
    return {
      ...jest.requireActual("react"),
      useRef: () => {
        return React.createRef<HTMLDialogElement | null>();
      }
    };
});


describe("<Modal />", () => {

    test("Test open modal element", async () => {
        await act( async () => 
            await render(
                <Modal open={true} closeModal={onClose}>
                    <div>
                    </div>
                </Modal>
            )
        );
    });

    test("Test close modal element", async () => {
        await act( async () => 
            await render(
                <Modal open={false} closeModal={onClose}>
                    <div>
                    </div>
                </Modal>
            )
        );

    });
});