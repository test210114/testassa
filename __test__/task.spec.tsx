import React from "react";
import { render, fireEvent, act } from "@testing-library/react";
import '@testing-library/jest-dom';
import {Task} from "../src/component/task";
import { renderWithProviders } from "./test-util";

const mockedUsedNavigate = jest.fn();
jest.mock('react-router-dom', () => ({
...jest.requireActual('react-router-dom') as any,
useNavigate: () => mockedUsedNavigate,
}));

jest.mock("react", () => {
    return {
      ...jest.requireActual("react"),
      useRef: () => {
        return React.createRef();
      }
    };
});

const mockSetState = jest.fn();
jest.mock('react', () => ({
    ...jest.requireActual('react'),
    useState: (initial: object) => [initial, mockSetState]
}));

describe("<Task />", () => {

    test("Test task element", async () => {
        const taskElement = await renderWithProviders(<Task />);
        const taskButton = taskElement.getByText('Regresar');
        expect(taskButton).toBeInTheDocument();
        fireEvent.click(taskButton);
        expect(mockedUsedNavigate).toHaveBeenCalled();
        
        const addButton = taskElement.getByText('Agregar');
        expect(addButton).toBeInTheDocument();
        fireEvent.click(addButton);
        expect(mockedUsedNavigate).toHaveBeenCalled();
    });
    
});